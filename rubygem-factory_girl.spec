%global gem_name factory_girl

Name: rubygem-%{gem_name}
Version: 4.8.0
Release: 1%{?dist}
Summary: Framework and DSL for defining and using model instance factories
License: MIT
URL: https://github.com/thoughtbot/factory_girl
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/thoughtbot/factory_girl.git && cd factory_girl
# git checkout v4.8.0 && tar czvf factory_girl-4.8.0-specs.tar.gz spec/
Source1: %{gem_name}-%{version}-specs.tar.gz
# git clone https://github.com/thoughtbot/factory_girl.git && cd factory_girl
# git checkout v4.8.0 && tar czvf factory_girl-4.8.0-features.tar.gz features/
Source2: %{gem_name}-%{version}-features.tar.gz
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(rspec-its)
BuildRequires: rubygem(activerecord)
BuildRequires: rubygem(mocha) >= 0.12.8
BuildRequires: rubygem(sqlite3)
BuildRequires: rubygem(timecop)
BuildRequires: %{_bindir}/cucumber
BuildRequires: rubygem(aruba)
BuildArch: noarch

%description
Framework and DSL for defining and using factories - less error-prone,
more explicit, and all-around easier to work with than fixtures.

%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version} -b 1 -b 2

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/



%check
pushd .%{gem_instdir}
# Move test into the place.
ln -s %{_builddir}/spec spec

# We don't care about coverage.
sed -i "/simplecov/ s/^/#/" spec/spec_helper.rb

# We don't have bourne in Fedora and it deprecated upstream:
# https://github.com/thoughtbot/bourne/pull/36
# There is aslo PR removing the dependency on Mocha and Bourne:
# https://github.com/thoughtbot/factory_girl/pull/1016
# Lets get rid of Bourne and the failures for now.
sed -i '/bourne/ s/^/#/' spec/spec_helper.rb
sed -i '/have_received\(.*\)\.never/ s/^/#/' spec/acceptance/attributes_from_instance_spec.rb
sed -i '/have_received\(.*\)\.once/ s/^/#/' \
  spec/factory_girl/definition_spec.rb \
  spec/factory_girl/factory_spec.rb
sed -i '/load_definitions_from(.*)/ s/^/#/' spec/factory_girl/find_definitions_spec.rb
sed -i '/have_received\(.*\)\.with/ s/^/#/' \
  spec/factory_girl/attribute/association_spec.rb \
  spec/factory_girl/declaration_list_spec.rb \
  spec/factory_girl/definition_proxy_spec.rb \
  spec/factory_girl/definition_spec.rb \
  spec/factory_girl/disallows_duplicates_registry_spec.rb \
  spec/factory_girl/factory_spec.rb \
  spec/factory_girl/strategy_calculator_spec.rb \
  spec/support/shared_examples/strategy.rb

rspec -rfileutils spec

# Move features into the place.
ln -s %{_builddir}/features features

# We don't care about coverage.
sed -i "/simplecov/ s/^/#/" features/support/env.rb

# Compatibility with Rails 5.1.
# https://github.com/thoughtbot/factory_girl/pull/1013
sed -i "s/ActiveRecord::Migration/ActiveRecord::Migration[4.2]/" features/support/factories.rb

cucumber
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE
%exclude %{gem_instdir}/factory_girl.gemspec
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Appraisals
%doc %{gem_instdir}/CONTRIBUTING.md
%doc %{gem_instdir}/GETTING_STARTED.md
%{gem_instdir}/Gemfile*
%doc %{gem_instdir}/NAME.md
%doc %{gem_instdir}/NEWS
%doc %{gem_instdir}/README.md
%{gem_instdir}/gemfiles
%{gem_instdir}/Rakefile
%{gem_instdir}/cucumber.yml

%changelog
* Wed Jul 12 2017 Vít Ondruch <vondruch@redhat.com> - 4.8.0-1
- Update to factory_girl 4.8.0.

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2.3.2-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2.3.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Mar 06 2013 Vít Ondruch <vondruch@redhat.com> - 2.3.2-6
- Rebuild for https://fedoraproject.org/wiki/Features/Ruby_2.0.0

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon Feb 06 2012 Bohuslav Kabrda <bkabrda@redhat.com> - 2.3.2-3
- Rebuilt for Ruby 1.9.3.

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 13 2011 Michal Fojtik <mfojtik@redhat.com> - 2.3.2-1
- Version bump

* Tue Jul 05 2011 Chris Lalancette <clalance@redhat.com> - 1.3.2-5
- Fixes to build in rawhide

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Oct 14 2010 Michal Fojtik <mfojtik@redhat.com> - 1.3.2-3
- Replaced path with path macro

* Wed Oct 13 2010 Michal Fojtik <mfojtik@redhat.com> - 1.3.2-2
- Rakefile fixing moved to a separate patch
- Fixed unneeded Requires
- Fixed directory ownership on doc subpackage
- README and LICENSE moved back to main package

* Sat Oct 02 2010 Michal Fojtik <mfojtik@redhat.com> - 1.3.2-1
- Initial package
